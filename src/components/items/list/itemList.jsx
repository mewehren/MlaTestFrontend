import React from 'react';

import { ItemComponent } from "./item.jsx";
import queryString from "query-string";

import { Categories } from "../categories.jsx";
import { NoResultView } from '../../utils/noResults.jsx';

export class ItemList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            items: [],
            noResults: false
        };
    }

    render() {
        const items = this.state.items;
        const categories = this.state.categories;
        console.log(this.state)
        if (items && items.length) {
            let list = items.map((item) => {
                return <ItemComponent
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    price={item.price.amount}
                    decimals={item.price.decimals}
                    picture={item.picture}
                    condition={item.condition}
                    free_shipping={item.free_shipping}
                    address={item.address}
                />;
            });
            return (<div className="row no-gutters">
                <Categories className='col-12' categories={categories} />
                <div className='col-12 searchResults'>
                    {list}
                </div>
            </div>);
        } else if (this.state.noResults) {
            return <NoResultView />
        } else return null;
    }

    componentDidMount() {
        if (this.getQuery()) {
            this.getItems(4);
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location.search != this.props.location.search) {
            this.setState({
                items: [],
                categories: [],
                noResults: false
            });
            this.getItems(4);
        }
    }

    getItems(limit) {
        var l = limit ? limit : 0;
        const query = this.getQuery();
        fetch('/api/items?q=' + query + '&l=' + l)
            .then((results) => {
                return results.json()
            }).then((data) => {
                this.setState({
                    items: data.items,
                    categories: data.categories,
                    noResults: !!data.items
                });
            });
    }

    getQuery() {
        const search = queryString.parse(location.search);
        return search.search;
    }
}