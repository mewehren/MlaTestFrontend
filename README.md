# MlaTestFrontend

# Test práctico para aspirantes al área de Front-end de Mercado Libre.

# Instalar:

Node.js (v8.15.1)

### Stack Utilizado

* Node.js (v8.15.1)
* Express
* React.js
* React Router
* Webpack
* Babel
* Sass

### Comandos

# Clonar Proyecto en la carpeta deseada
$ git clone https://gitlab.com/tinwehren/MlaTestFrontend.git

# Posicionarse en la carpeta creada y Instalar dependencias

$ npm install

# Correr proyecto Dev

$ npm run dev 

# Abrir navegador en http://localhost:4000

# Production

$ npm run prod