import React from 'react';
import { ChevronRight } from 'react-bytesize-icons';


export class Categories extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const length = this.props.categories.length;
        var list = [];
        for (var i = 0; i < length - 1; i++) {
            const category = this.props.categories[i];
            const categorySpan = <span key={category}>{category}</span>
            const separatorSpan = <span key={'separator' + i}><ChevronRight height={10} width={10} strokeWidth='10.9375%' /></span>
            list.push(categorySpan);
            list.push(separatorSpan);
        }
        const last = this.props.categories[length - 1];
        const lastSpan = <span className='last' key={last}>{last}</span>
        list.push(lastSpan);
        return (
            <div className='categories' >
                {length ? list : <span>&nbsp;</span>}
            </div>
        );
    }
}