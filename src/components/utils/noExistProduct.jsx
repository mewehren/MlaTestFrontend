import React from 'react';
import { Link } from 'react-router-dom';

export function NoExistProduct() {
    return (
        <div className='col-12 no-result'>
            <div className="info">
                <h2 class="header__title"> Parece que la página no existe. </h2>
                <ul class="ch-btn-action">
                    <Link to='/'  >
                        <h3>Ir a la Página principal.</h3>
                    </Link>
                </ul>
            </div>
        </div>
    );
}

