import React from 'react';

export function NoResultView() {
    return (
        <div className='col-12 no-result'>
            <div className="info">
                <h3 class="header__title"> No hay publicaciones que coincidan con tu búsqueda. </h3>
                <ul class="links__list">
                    <li>Revisá la ortografía de la palabra.</li>
                    <li>Utilizá palabras más genéricas o menos palabras.</li>
                </ul>
            </div>
        </div>
    );
}

