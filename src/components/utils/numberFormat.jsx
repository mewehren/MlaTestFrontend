import React from 'react';

export function FormatNumber(props) {
    // Intl​.Number​Format Javascript
    const number = new Intl.NumberFormat('es').format(props.value);
    return (
        <span>{number}</span>
    );
}