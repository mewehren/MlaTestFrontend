const request = require('request');

const errorML = require('./errorML');

module.exports = function (req, res) {
    const query = req.query.q || '';
    const limit = req.query.l || 0;
    const endpoint = limit == 0 ? 'https://api.mercadolibre.com/sites/MLA/search?q='+query : 'https://api.mercadolibre.com/sites/MLA/search?q='+query+'&limit='+limit
    request(endpoint, function(error, response, body) {
        if (!error) {
            const data = JSON.parse(body);
            if (data.results) {
                var categories = [];

                if (data.filters.length) {
                    const { path_from_root: pathFromRoot } = data.filters[0].values[0]
                    pathFromRoot.map(category => categories.push(category.name))
                }
                
                var items = data.results.map((item) => {                    
                    const amount = Math.floor(item.price);
                    const decimals = +(item.price%1).toFixed(2).substring(2);
                    return {
                        id: item.id,
                        title: item.title,
                        price: {
                            currency: item.currency_id,
                            amount: amount,
                            decimals: decimals
                        },
                        picture: item.thumbnail,
                        condition: item.condition,
                        free_shipping: item.shipping ? item.shipping.free_shipping : false,
                        address: item.address ? item.address.state_name : ''
                    }
                });

                

                const list = {
                    author: {
                        name: 'Martin Ezequiel',
                        lastname: 'Wehren'
                    },
                    categories: categories,
                    items: items
                }                
                res.send(list);
            }
        } else {
            res.send(errorML);
        }
    });
}