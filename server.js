const express = require('express');
const path = require('path');

const port = 4000;
const server = express();
const api = require('./api/api.js');

server.use(express.static(__dirname + '/public'));

server.use('/api', api);

server.get('*', (req, res) =>
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'))
);

server.listen(port);

console.log("Server listening on port: ", port);